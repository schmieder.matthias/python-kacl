# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.6.2] - 2024-10-23
### Added
- added support for yaml metadata in `Changelog.md`

## [0.6.1] - 2024-07-22
### Fixed
- fixed issue with JIRA comment renderings, where `**bold**` and monospaced code in single ticks was not shown correctly

## [0.6.0] - 2024-07-20
### Added
- added support to create comments on issue management systems (JIRA)

## [0.5.1] - 2024-07-04
### Fixed
- fixed handling of `post` releases where it was not possible to release post versions without a former version with same base version

### Added
- added tests

## [0.5.0] - 2024-07-03
### Fixed
- integrated !34 to fix documentation errors

### Added
- Allow to release a version even if there are no changes by passing `--allow-no-changes`. Contrib: Andrii Hrishchenko !35
- Added `next` command that allows to print the next version that will be released. Contrib: Markus Heberling Hrishchenko !36

## [0.4.7] - 2024-07-03
### Fixed
- fixed bug where `post_release_version_prefix` was not used when incrementing through `post`

## [0.4.7] - 2024-03-08
### Fixed
- Added `--allow-no-changes` flag to the `kacl-cli release` command to allow releases without any changes

## [0.4.6] - 2023-06-13
### Fixed
- fixed bug that prevented the committing of changes to git

## [0.4.5] - 2023-05-26
### Fixed
- Fixed `dockerhub_publish_description` CI job

## [0.4.4] - 2023-05-26
### Added
- Added `kacl-cli squash` to squash releases

## [0.4.3] - 2023-05-24
### Added
- added Dockerhub description publishing

### Removed
- removed obsolete scripts

## [0.4.2] - 2023-05-23
### Added
- added check, that every entry belongs to a subsection
- added check, that there are no duplicate subsections
- removed case-sensitivity on link reference definitions according to [markdown specification](https://spec.commonmark.org/0.30) (see [example 205](https://spec.commonmark.org/0.30/#example-205)).

## [0.4.1] - 2023-05-21
### Fixed
- fixed `docker.io` deployment
- better SonarCloud Python Version indications

## [0.4.0] - 2023-05-21
### Changed
- migration to GitLab.com

## [0.3.4] - 2023-01-24
### Fixed
- fixed issue, where config was not loaded correctly when passend by `-c,--config`

## [0.3.3] - 2023-01-16
### Fixed
- fixed issue where pip install did not work

## [0.3.2] - 2023-01-16
### Changed
- removed `python-box` dependency
- `kacl-cli` won't enable the `post-release` feature on default
- improved config system and python code maintainability

### Added
- added `post_release_version_prefix` config variable
- added docs for `post-release` feature

## [0.3.1] - 2023-01-15
### Fixed
- Fixed CI to use VERSION file

### Changed
- Changelogs will always have a newline at the end of a file after being dumped

### Added
- added support for `post` releases that can be used to indicate changes on a released patch version that is in production

## 0.2.29 - 2022-05-09
### Added
- added `--no-commit` option
- added `pre-commit` hook

### Fixed
- fixed handling of markdown newlines

## 0.2.23 - 2021-01-14
### Added
- added `auto_generate` option to `links` section in config
- added console command to get the latest version from the Changelog

### Fixed
- Fixed typos
- Fixed #7 where link generation failed if no version was available
- fixed issues with auto generation of links where unrelease template was wrong
- fixed config hierarchy
- If running on gitlab-ci and no host URL is configured the URL is derived from CI_PROJECT_URL
- Fixed #17 where a single newlines was interpreted as a paragraph split, going against the markdown spec

## 0.2.17 - 2020-01-14
### Added
- `release` command will make sure changelog is valid before doing any changes.
- It is now possible to automatically generate links for versions using `kacl-cli link generate` or `kacl-cli release patch --auto-link`

## 0.2.16 - 2020-01-07
### Fixed
- fixed issue #3 that did not detect linked versions with missing links
- fixed issue #2 that caused errors on files with CRLF endings.

## 0.2.15 - 2020-01-06
### Fixed
- Fixed bug where configs where not merged recursively

## 0.2.14 - 2020-01-06
### Fixed
- fixed bug where default config was not copied into the sdist package
- fixed issue where help could not be retrieved for commands if no valid CHANGELOG file was given

## 0.2.13 - 2020-01-05
### Added
- added config file support
- added CLI tests for `verify` command

## 0.2.12 - 2020-01-02
### Fixed
- Validation reported success even if versions did contain information outside of change sections

## 0.2.11 - 2019-12-26
### Fixed
- Fixed issue where description on dockerhub was not updated when deploying

## 0.2.10 - 2019-12-26
### Fixed
- issue where build had been triggered reoccuringly whenever Travis built the master branch

## 0.2.9 - 2019-12-26
### Fixed
- CI behaviour on master branch

## 0.2.8 - 2019-12-26
### Added
- added git support to the `release` command. When using `--tag/--commit` changes will be tracked by git
- added support for simple version increment. User `kacl-cli release [major|minor|patch]`

## 0.2.7 - 2019-12-25

## 0.2.6 - 2019-12-23
### Fixed
- fixed issue where `--version` resulted in error if no CHANGELOG.md file was found within the execution directory

## 0.2.5 - 2019-12-23
### Added
- added checks for `release` function that now checks if there are *available changes*, *already used version* and *valid semver in descending order*
- added default content checks
- cli will now check for valid semantic version when using `release` command
- implemented basic cli with `new`, `get`, `release`, `verify`
- added `--json` option to `verify` command

[Unreleased]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.6.2...HEAD
[0.6.2]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.6.1...v0.6.2
[0.6.1]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.6.0...v0.6.1
[0.6.0]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.5.1...v0.6.0
[0.5.1]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.5.0...v0.5.1
[0.5.0]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.4.7...v0.5.0
[0.4.7]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.4.6...v0.4.7
[0.4.7]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.4.6...v0.4.7
[0.4.6]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.4.5...v0.4.6
[0.4.5]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.4.4...v0.4.5
[0.4.4]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.4.3...v0.4.4
[0.4.3]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.4.2...v0.4.3
[0.4.2]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.3.4...v0.4.0
[0.3.4]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.3.3...v0.3.4
[0.3.3]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.3.2...v0.3.3
[0.3.2]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.3.1...v0.3.2
[0.3.1]: https://gitlab.com/schmieder.matthias/python-kacl/-/compare/v0.2.29...v0.3.1
