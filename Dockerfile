FROM python:3.11-alpine

RUN apk update && apk add git --no-cache

COPY . /src

RUN cd src && pip install . \
    && rm -rf /src

ENTRYPOINT [ "kacl-cli" ]